package assignment5;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;

public class A5Driver
{
    public static void main(String[] args)
    {
    	 StopWatch s = new StopWatch();
         int i;
         long mytime;
         s.start( );
        // Create a word ladder solver object
        Assignment5Interface wordLadderSolver = new WordLadderSolver();

        // Read file in
        // for each line in the file call compute ladder

        try
        {
	        FileInputStream in = new FileInputStream("src/assignment5/assn5data.txt");
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
	        String strLine;
	         
	        while((strLine = br.readLine()) != null)
	        {
	        	int index = strLine.indexOf(' ');
	        	String startWord = strLine.substring(0, index);
	        	String temp = strLine.substring(index);
	        	String endWord = temp.replaceAll("\\W", "");
		        try 
		        {
		            List<String> result = wordLadderSolver.computeLadder(startWord, endWord);
		            boolean correct = wordLadderSolver.validateResult(startWord, endWord, result);
		            if(correct)
		            {
		            	Iterator<String> resultItr = result.iterator();
		            	while(resultItr.hasNext())
		            	{
		            		System.out.println(resultItr.next());
		            	}
		            }
		            else
		            {
		            	//placeholder for exception
		            	System.out.println("invalid ladder");
			        	System.out.println("**********");
		            }
		        } 
		        catch (NoSuchLadderException e) 
		        {
		            e.printStackTrace();
		        	System.out.println("**********");
		        }
		        catch (IllegalWordException e)
		        {
		        	System.out.println(e);
		        	System.out.println("**********");
		        }
	        }
        }
	    catch(Exception e)
	    {
	    	System.out.println(e);
	    }
        s.stop();
        mytime=s.getElapsedTime();
        System.out.println("Time elapsed in nanoseconds is: "+ mytime);
        System.out.println("Time elapsed in seconds is: "+ mytime/ s.NANOS_PER_SEC);
    }
	        
}
