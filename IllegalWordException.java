package assignment5;

public class IllegalWordException extends Exception
{
		public IllegalWordException(String message)
		{
			super(message);
		}
}
