/*
    Horton, Cody
    CBH875
    
    Akshans, Verma
    
    Arman, Mirpour
    
    
    Assignment 5
 */

package assignment5;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

// do not change class name or interface it implements
/**
 * 
 * WordLadderSolver contains a dictionary of legal 5 letter words to be used in a word ladder,
 * and solutionList contains the resulting word ladder constructed when given 2 words
 * 
 * @author Cody Horton, Akshans Verma, Arman Mirpour
 * 
 */
public class WordLadderSolver implements Assignment5Interface
{
    // declare class members here.
	private HashSet<String> dictionary;
	private List<String> solutionList;
	
    // add a constructor for this object. HINT: it would be a good idea to set up the dictionary there
	/**
	 * 
	 * The constructor for a WordLadderSolver object. Initializes the dictionary using the file "assn5words.dat"
	 * located in the src/assignment5 folder.
	 * 
	 * @author Cody Horton, Akshans Verma, Arman Mirpour
	 * 
	 */
	public WordLadderSolver()
	{
		try
        {
			dictionary = new HashSet<String>();
	        FileInputStream in = new FileInputStream("src/assignment5/assn5words.dat");
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
	        String strLine;
	        
	        while((strLine = br.readLine()) != null)
	        {
	        	String ignore = strLine.substring(0, 1);
	        	if(!ignore.equals("*"))
	        	{
	        		dictionary.add(strLine.substring(0,5));
	        	}
	        }
        }
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
    // do not change signature of the method implemented from the interface
	/**
	 * Computes a word ladder from startWord to endWord
	 * First both parameters are checked for validity. Then makeLadder is called which creates the ladder.
	 * 
	 * @param startWord is the beginning of the word ladder
	 * @param endWord is the end of the word ladder
	 * @return The word ladder that joins the 2 parameters together
	 * @throws NoSuchLadderException is thrown when no ladder can be made
	 * @throws IllegalWordException when an invalid word due to not being found in the dictionary is used as a parameter
	 * 
	 */
    @Override
    public List<String> computeLadder(String startWord, String endWord) 
    		throws NoSuchLadderException, IllegalWordException 
    {
    		solutionList = new ArrayList<String>();
        	valid(startWord);
        	valid(endWord);
        	if(!makeLadder(startWord, endWord, -1))
        	{
        		throw new NoSuchLadderException("word ladder does not exist");
        	}
        	solutionList.add("**********");
			return solutionList; 
    }

    /**
     * validateResult checks the word ladder to ensure the beginning and end of the ladder are the start and end word,
     * that each word in the ladder is in the dictionary, and that there are no duplicates.
     * 
     * @param startWord beginning of word ladder
     * @param endWord end of the word ladder
     * @param wordLadder the word ladder to be checked for validity
     * @returns true if the word ladder is valid, false otherwise
     * 
     */
    @Override
    public boolean validateResult(String startWord, String endWord, List<String> wordLadder) 
    {
	    if(!startWord.equals(endWord))
	    {

    		Set<String> set = new HashSet<String>(wordLadder);
	
	    	if(set.size() < wordLadder.size())
	    	{ 
	    		System.out.println("Duplicates");
	    		return false; 
	    	}
	    }
        if(!wordLadder.get(0).equals(startWord))
        { 
        	System.out.println("startWord != fromWord");
        	return false; 
        }
        
        if(!wordLadder.get(wordLadder.size() - 2).equals(endWord))
        { 
        	System.out.println("endWord != toWord");
        	return false; 
        }
        
        Iterator<String> ladderItr = wordLadder.iterator();
        while(ladderItr.hasNext())
        { 
        	String current = ladderItr.next();
        	if(!dictionary.contains(current) & !current.equals("**********"))
        	{
        		System.out.println("invalid word used in ladder");
        		return false;
        	}
        }
        return true;
    }

    /**
     * makeLadder recursively calculates a wordladder that connects fromWord to toWord. First fromWord is added to the solution.
     * Then a list of all words in the dictionary that can be made by changing one letter of fromWord, besides the letter in the same
     * position as was previously changed, will be added to a temporary list. Those words already included in the solution will not 
     * be in the temporary list. Recursive calls using each element in the list as the new fromWord parameter and the position of the 
     * letter changed as the new pos parameter will be made until either a word ladder is found or it can be determined no such ladder
     * exists.
     * 
     * @param fromWord the word from which all possible 1 letter difference words are determined
     * @param toWord the word in which we are trying to approach from fromWord
     * @param pos the index of the position of the letter that was most recently changed
     * @return true if a ladder can be made, false if not
     */
    private boolean makeLadder(String fromWord, String toWord, int pos)
    {
        	solutionList.add(fromWord);
        	if(fromWord.equals(toWord))
        	{
        		solutionList.add(toWord);
        		return true;
        	}
        	int nextPos = -1;
        	int fromDifference;
        	int toDifference;
        	String dictEntry;
        	List<String> tempList = new ArrayList<String>();
        	Iterator<String> dicItr = dictionary.iterator();
        	while(dicItr.hasNext())
        	{
        		fromDifference = 0;
        		toDifference = 0;
        		dictEntry = dicItr.next();
        		for(int k = 0; k < dictEntry.length(); k++)
        		{
        			if(k == pos){ continue; }
        			if(fromWord.charAt(k) != dictEntry.charAt(k))
        			{
        				fromDifference++;
        				nextPos = k;
        			}
        			if(toWord.charAt(k) != dictEntry.charAt(k))
        			{
        				toDifference++;
        			}
        			
        		}
        		if(fromDifference == 1)
        		{
        			if(!solutionList.contains(dictEntry))
        			{
        				tempList.add(toDifference + dictEntry);
        			}
        			
        		}
        		
        	}
        	Collections.sort(tempList);
        	for(String temp : tempList)
        	{
        		
        		if(temp.charAt(0) == '0')
        		{
        			solutionList.add(temp.substring(1));
        			return true;
        		}
        		if(makeLadder(temp.substring(1), toWord, nextPos))
        		{
        			return true;
        		}
        	}
        	solutionList.remove(solutionList.size() - 1);
        	return false;
    }
    
    public void valid(String word)
        	throws IllegalWordException
    {
        	if(word.length() != 5)
        	{
        		throw new IllegalWordException("Incorrect word length"); 
        	}
        	if((!dictionary.contains(word)) & (word != "**********"))
        	{
        		throw new IllegalWordException("Word not contained in dictionary");
        	}
        		
    }
    public boolean dictionaryContains(String word)
    {
    	if(dictionary.contains(word)){ return true; }
    	return false;
    }
    
}
